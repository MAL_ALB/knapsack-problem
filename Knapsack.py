# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 11:09:14 2020

@author: Marcus
"""

import random
import math
import time

# -- Variables --
n = 21  # number of available items
m = 1 / n  # mutation rate
pop = 50  # number of solutions per Round
size_of_pack = 863
survivors = math.floor(pop / 4)
mutation_resitant_solutions = 1  # amount of solutions, that are untuched by mutations
evolutionary_cycles = 100
show_evolving_process = False
mutation_rate = 3 / n
parent_number = 4  # A next gen solution inherits from this much solutions (Has to be smaler than number og genes)


# Create a number of items, the solutin can be build from
def create_probleme():
    problem = []
    for j in range(n):
        problem.append(random.randint(1, round(size_of_pack / (n / 4) + size_of_pack / 4)))
    return problem


# Create a random solutin for the start
def create_initial_solutions():
    solutions = []
    for i in range(pop-1):
        solution = []
        for j in range(n):
            solution.append(random.randint(0,1))
        solutions.append(solution)
    min_sol = [0]*n
    min_sol[random.randint(0,n-1)] = 1
    solutions.append(min_sol)
    return solutions


# Score the solutions
def fittnes(solution, problem):
    score = 0
    for i in range(n):
        if solution[i]: score += problem[i]
    if score > size_of_pack:
        return 0
    else:
        return score


# Sort for topscorers and select them
def selection(solutions, fitness):
    survv_solutions = []
    zipped = zip(solutions, fitness)
    zipped = sorted(zipped, key=lambda t: t[1])
    for i in range(survivors):
        survv_solutions.append(zipped[-(i + 1)][0])
    return survv_solutions


# generate new solutions by crossing over genes
def crossover(solutions):
    crsd_sol = []
    cut_locations = random.sample(range(0, len(solutions[0])), parent_number - 1)
    cut_locations.sort()
    # for i in range(parent_number-1):
    #     cut_locations.append()#random.randint(0,len(solutions[0])-1)
    cut_locations.append(len(solutions[0]))
    for i in range(len(solutions)):
        crsd_sol.append([])
    for i in range(len(solutions)):
        mem = 0
        for j in range(parent_number):
            take_from = (j + i) % len(solutions)
            crsd_sol[i] += solutions[take_from][mem:cut_locations[j]]
            mem = cut_locations[j]
    return crsd_sol


def calc_mutation_rate(probability):
    return random.random() < probability


# Randomly flip genes
def mutate(solutions):
    mutated = []
    for i in range(len(solutions)):
        mutated.append([])
        for j in range(len(solutions[i])):
            if calc_mutation_rate(mutation_rate):
                if solutions[i][j] == 1:
                    mutated[i].append(0)
                else:
                    mutated[i].append(1)
            else:
                mutated[i].append(solutions[i][j])
    return mutated


def create_next_gen(survivors):
    new_gen = survivors  # Elitism
    crossed = crossover(survivors)
    for crss in crossed:  # Crossover genes
        new_gen.append(crss)
    crossed = crossover(survivors)
    for crss in crossed:
        new_gen.append(crss)
    new_gen[mutation_resitant_solutions:] = mutate(new_gen[mutation_resitant_solutions:])
    new_sol = create_initial_solutions()
    i = 0
    while len(new_gen) < pop:
        new_gen.append(new_sol[i])
        i += 1
    return new_gen


def solve_by_genetic(problem):
    info_every_n_iteration = evolutionary_cycles / 4
    solutions = create_initial_solutions()
    for z in range(evolutionary_cycles):
        if z % (info_every_n_iteration - 1) == 0 and show_evolving_process:
            print('Generation: ', z, '\n\n')
        fitnesses = []

        for i in range(pop):
            fitnesses.append(fittnes(solutions[i], problem))
            if z % (info_every_n_iteration - 1) == 0 and show_evolving_process:
                print(solutions[i], '   ', fitnesses[i], '\n')
        survivv = selection(solutions, fitnesses)
        solutions = create_next_gen(survivv)
    return solutions[0], fitnesses[0]


# -- Classic solving --

def int_to_binary(number):
    binary_list = []
    while (True):
        if (number > 1):
            binary_list.append(number % 2)
            number = math.floor(number / 2)
        elif (number == 1):
            binary_list.append(1)
            return binary_list
        else:
            binary_list.append(0)
            return binary_list


def binary_to_int(bin_num):
    number = bin_num[0]
    for i in reversed(range(1, len(bin_num))):
        number += (bin_num[i] * 2) ** i
    return number


def solve_by_iteration(problem):
    combin = binary_to_int([1] * n)
    score = 0
    solution = []
    for i in range(combin):
        sol = int_to_binary(i)
        while (len(sol) < n):
            sol.append(0)
        if fittnes(sol, problem) > score:
            score = fittnes(sol, problem)
            solution = sol
    return solution, score


def solve_with_bnb(problem):
    size = size_of_pack
    _p_num = len(problem)

    def solve_with_bnb_rek(_index, _curr):
        if _index == _p_num:
            return _curr, []
        elif _curr == size:
            return _curr, [0] * (_p_num - _index)
        elif _curr + problem[_index] <= size:
            s1, c1 = solve_with_bnb_rek(_index + 1, _curr + problem[_index])
            s2, c2 = solve_with_bnb_rek(_index + 1, _curr)
            if s1 > s2:
                return s1, c1 + [1]
            else:
                return s2, c2 + [0]
        else:
            s, c = solve_with_bnb_rek(_index + 1, _curr)
            return s, c + [0]

    s, c = solve_with_bnb_rek(0, 0)
    return list(reversed(c)), s


def solve_dynamically(problem):
    size = size_of_pack
    p_num = len(problem)
    # m[i][j] maximum weight with items up to the i-th and weight up to j
    m = []
    m.append([])
    for j in range(size + 1):
        if problem[0] <= j:
            m[0].append(problem[0])
        else:
            m[0].append(0)
    for i in range(1, p_num):
        m.append([])
        for j in range(size+1):
            if problem[i] > j:
                m[i].append(m[i - 1][j])
            else:
                m[i].append(max(m[i - 1][j], m[i - 1][j - problem[i]] + problem[i]))
    return m[p_num-1][size]


if __name__ == "__main__":
    problem = create_probleme()
    print('Problem: ',problem, '\n\n')
    print('Genetic Solving: \n')
    start = time.time()
    solution, score = solve_by_genetic(problem)
    end = time.time()
    print(' Score: ', (score / size_of_pack) * 100, '%\n Runntime: ', round((end - start) * 1000, 1), 'ms',
          '\n Solution: \n', solution, '\n')

    print('Iterative Solving: \n')
    start = time.time()
    solution, score = solve_by_iteration(problem)
    end = time.time()
    print(' Score: ', score, '\n Runntime: ', round((end - start), 1), 's', '\n Solution: \n', solution, '\n')

    print('BnB Solving: \n')
    start = time.time()
    solution, score = solve_with_bnb(problem)
    end = time.time()
    print(' Score: ', (score / size_of_pack) * 100, '%\n Runntime: ', round((end - start) * 1000, 1), 'ms',
          '\n Solution: \n', solution, '\n')

    print('Dynamic Solving: \n')
    start = time.time()
    score = solve_dynamically(problem)
    end = time.time()
    print(' Score: ', (score / size_of_pack) * 100, '%\n Runntime: ', round((end - start) * 1000, 1), 'ms',
          '\n')
